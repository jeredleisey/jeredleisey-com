export default defineNuxtConfig({
  devtools: { enabled: true },
  css: ['@/assets/css/main.css'],
  modules: [
    '@nuxtjs/tailwindcss',
    'nuxt-particles',
    '@nuxtjs/device',
    [
      '@storyblok/nuxt',
      {
        accessToken: import.meta.env.NUXT_STORYBLOK_PUBLIC_ACCESS_TOKEN,
        devtools: true,
        apiOptions: { region: 'us' },
      },
    ],
  ],
  runtimeConfig: {
    public: {},
  },
  particles: {
    mode: 'slim',
    lazy: true,
  },
});
