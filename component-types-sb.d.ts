import {StoryblokStory} from 'storyblok-generate-ts'

export interface AssetStoryblok {
  alt?: string;
  copyright?: string;
  id: number;
  filename: string;
  name: string;
  title?: string;
  focus?: string;
  [k: string]: any;
}

export interface ArticleStoryblok {
  header_image?: AssetStoryblok;
  title?: string;
  content?: any[];
  _uid: string;
  component: "article";
  [k: string]: any;
}

export interface ArticleParagraphStoryblok {
  content?: string;
  _uid: string;
  component: "article-paragraph";
  [k: string]: any;
}

export interface FeatureStoryblok {
  name?: string;
  _uid: string;
  component: "feature";
  [k: string]: any;
}

export interface FigureStoryblok {
  image?: AssetStoryblok;
  label?: string;
  _uid: string;
  component: "figure";
  [k: string]: any;
}

export interface GridStoryblok {
  columns?: any[];
  _uid: string;
  component: "grid";
  [k: string]: any;
}

export interface LatestArticlesStoryblok {
  articles?: (StoryblokStory<ArticleStoryblok> | string)[];
  _uid: string;
  component: "latest-articles";
  [k: string]: any;
}

export interface PageStoryblok {
  body?: any[];
  _uid: string;
  component: "page";
  uuid?: string;
  [k: string]: any;
}

export interface TeaserStoryblok {
  headline?: string;
  _uid: string;
  component: "teaser";
  [k: string]: any;
}
