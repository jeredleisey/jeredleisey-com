import type { Config } from 'tailwindcss';

export default {
  content: [
    'storyblok/**/*.{vue,js}',
    'components/**/*.{vue,js}',
    'pages/**/*.vue',
  ],
  theme: {
    extend: {
      backgroundImage: {
        carina: "url('/carina_nircam.jpg')",
        bubble: "url('/bubble-chamber-tracks.png')",
      },
      colors: {
        my: {
          patina: '#CDCDC3',
          bronze: '#CAA45D',
          gold: '#CC922F',
          slate: '#1E3957',
          royal: '#0A2749',
          midnight: '#0F1F2F',
        },
      },
      fontFamily: {
        inter: 'Inter',
        'neue-montreal': 'Neue-Montreal',
      },
      spacing: {
        'pad-2': 'max(20px, 4vmin)',
        'pad-4': 'max(40px, 8vmin)',
      },
      backgroundSize: {
        'size-200': '200% 200%',
      },
      backgroundPosition: {
        'pos-0': '0% 0%',
        'pos-100': '100% 100%',
      },
    },
  },
  plugins: [require('@tailwindcss/typography')],
} satisfies Config;
